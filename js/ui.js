const todo = document.querySelector(".todo");

document.addEventListener("DOMContentLoaded", function () {
  // nav menu
  const menus = document.querySelectorAll(".side-menu");
  M.Sidenav.init(menus, { edge: "right" });
  // add todo form
  const forms = document.querySelectorAll(".side-form");
  M.Sidenav.init(forms, { edge: "left" });
});

// render todo data
export const renderTodo = (data) => {
  const html = `
  <div>
  ${data.map((todo) =>
    todo.state_data !== "deleted"
      ? `<div class="card-panel todo white row " data-id="${todo.id}">
      <img src="/img/valid.png" alt="todo thumb">
      <div class="todo-details">
        <div class="todo-title">${todo.title}</div>
        <div class="todo-description">${todo.description}</div>
      </div>
      <div id="todo-delete" class="todo-delete">
        <i class="material-icons" data-id="${todo.id}">delete_outline</i>
      </div>
    </div>`
      : ""
  )}
  </div>
  `;
  todo.innerHTML = html;
};
