import { renderTodo } from "./ui";
import {
  createTodo,
  getTodos,
  getTodo,
  deleteTodo as deleteTodoIDB,
} from "./idb";

window.addEventListener("online", syncLocalTodo());

async function syncLocalTodo() {
  let todos = await getTodos();
  let result = false;

  todos.forEach(async (todo) => {
    switch (todo.state_data) {
      case "created":
        result = await store(todo.id, todo);

        if (result) {
          todo.state_data = "sync";
          await createTodo(todo);
        }

        break;

      case "deleted":
        result = await remove(todo.id);
        if (result) {
          await deleteTodoIDB(todo.id);
        }
        break;

      // case "updated":
      //   console.log("update");
      //   break;

      // case "sync":
      //   console.log("data already sync");
      //   break;
    }
  });
}

export const get = () => {
  return fetch("http://localhost:3030/todos", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .catch((error) => console.log(error));
};

const form = document.querySelector("form");
form.addEventListener("submit", async (evt) => {
  evt.preventDefault();

  let id = Math.floor(Math.random() * Math.floor(Math.random() * Date.now()));
  let todo = {
    id: id,
    title: form.title.value,
    description: form.description.value,
    state_todo: "not_started",
    state_data: navigator.onLine ? "sync" : "created",
    date: Date.now(),
  };
  await syncLocalTodo();
  await createTodo(todo);

  if (navigator.onLine) {
    store(id, todo);
  } else {
    await render();
  }

  form.title.value = "";
  form.description.value = "";
});

async function render() {
  let todos = navigator.onLine ? await get() : await getTodos();
  renderTodo(todos);
}

const todoContainer = document.querySelector(".todo");
todoContainer.addEventListener("click", async (evt) => {
  evt.preventDefault();
  if (evt.target.tagName === "I") {
    const id = evt.target.getAttribute('data-id');
    if (window.confirm("Supprimer cette tâche ?")) {
      if(navigator.onLine) {
        await syncLocalTodo();
        await deleteTodoIDB(id);
        await remove(id);
      } else {
        let todo = await getTodo(id)
        await createTodo({
          id: todo.id,
          title: todo.title,
          description: todo.description,
          state_todo: todo.state_todo,
          state_data: "deleted",
          date: todo.date,
        });
        await render();
      }
    }
  }
});

function store(id, todo) {
  return fetch("http://localhost:3030/todos", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      id: id,
      title: todo.title,
      description: todo.description,
      state_todo: todo.state_todo,
      state_data: "sync",
      date: todo.date,
    }),
  }).then(async (response) => {
    await render();
    return true;
  });
}

async function remove(id) {
  return fetch("http://localhost:3030/todos/" + id, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then(async (response) => {
      await render();
      return true;
    })
    .catch((error) => console.log(error));
}
