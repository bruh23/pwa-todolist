import { renderTodo } from "./ui";
import {get} from "./db";

if ("serviceWorker" in navigator) {
    navigator.serviceWorker
      .register("/js/sw.js")
      .then(serviceWorker => {
        console.log("Service Worker registered: ", serviceWorker);
      })
      .catch(error => {
        console.error("Error registering the Service Worker: ", error);
      });
}


(async () => {
  let x = await get();
   renderTodo(x);
})();